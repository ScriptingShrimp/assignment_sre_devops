#!/usr/bin/env python3
import re
import sys

logfile = "/var/log/nginx/access.log"

ips = {}
ipRegex = r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"

with open(logfile) as f:
    for line in f:
        lineArray = line.split()
        if re.match(ipRegex, lineArray[0]):
            ip = lineArray[0]
            ips[ip] = ips.get(ip, 0) + 1

ipsSorted = sorted((value, key) for (key,value) in ips.items())

# fancy HTML -> write to file
print('<html>')
for (key, value) in (list(reversed(ipsSorted[-3:]))):
    print("IP:", value, "accesed", key, "times <br>")
print('</html>')