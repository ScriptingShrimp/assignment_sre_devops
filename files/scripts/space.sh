#!/bin/bash
set -euo pipefail

str=$(findmnt -P -o TARGET,USE% --pairs)
MNTARRAY=(`echo $str | tr ' ' '\n'`)

convert_usage () {
    if [[ $1 =~ [0-9] ]];then
        USEDIGIT=(`echo $1 | tr -cd [:digit:]`)
        FREECALC=(`expr 100 - $USEDIGIT`)
        echo $FREECALC%
    else
        # does not contain number
        echo "-"
    fi
}

for (( c=0; c<${#MNTARRAY[@]}; c=c+2 ))
do  
    even=$c
    odd=$c+1

    TARGET="${MNTARRAY[even]}"
    USE="${MNTARRAY[odd]}"

    TARGETVALUE=(`echo $TARGET | sed -n 's|.*TARGET="\([^"]*\)".*|\1|p'`)
    FREEPERCENT=`convert_usage $USE`

    echo "${TARGETVALUE}":"${FREEPERCENT}" $"<br>"
done

