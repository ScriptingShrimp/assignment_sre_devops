# SRE/DevOps assignment

**IMPORTANT:** this assignment reflects ~5 hours of my work



The goal of this assignment is to setup simple web server with few defined endpoints to demonstrate Ansible and simple scripting skills.

Expected output is a git repository with the Ansible code, scripts and README so it is possible to replicate the setup on new and fresh instance.

The code can be shared via email (zip, gzip or bare repo) or via private repository on Github.

There is a soft time-limit of four hours - this is just recommended time, if you do not manage to do everything in 4h don't panic, it can be enough to explain next steps in your words or with meta-code or just work few more hours.

## Introduction

Instance is open to the world on tcp ports 22, 80 and 443 and on icmp.

Please follow these principles while working on tasks:

- use Ansible or scripts to configure anything (packages, files, scripts) on the node
- use only Ansible "main" modules, do not use Galaxy modules
- use comments to describe not obvious parts
- if you think something is missing, wrong or requires further effor feel free to comment and move forward to another task
- feel free to get back with questions
- use git to control the code (Ansible, scripts, Dockerfile)

## Task 1 - Users

Create following users (no passwords), with usual home directories:

- name: john.doe
  ssh key: ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB6bpsEjbEdPaDEZXmNqz0qgHNzV1et0XL9xFWm9Kv1S joe.doe
  user can sudo without password

- name: sue.doe
	ssh key: ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGCTmcGOf/tticESph7TwVSNeX3AAkpfmCXpgDO5T4QZ joe.doe
	user *cannot* sudo

## Task 2 - Web server & simple scripts

Install web server of your choice (our recommendation is nginx).

Configure following:

- on `/cat` there is a page with a cat picture
- on `/dog` there is a page with a dog picture

Create a simple script (language of your choice) that will run at least every 5 minutes and will generate static endpoints:

- on `/uptime` publish output of `uptime` command
- on `/space` publish list of mountpoints and free space in format: `<mountpoint>: <free_bytes_perctent>%`
- on `/ips` publish list of top 3 IPs that accessed the server from access log (use only current log, ignore rotation)

Previous endopoints (`/uptime`, `/space` and `/ips`)  are accessible only for authorised users:

- username: jim.doe
  password: Zi0Aejai6ied0iem

- username: alice.doe
  password: thoi0ar3Joo3ni3o

## Task 3 - Docker

Build and start docker container on the instance:

- container should restart after instance reboots
- on port `3333` expose simple web page with "Lorem ipsum" content
- publish this page on instance's web server on `/docker` endpoint.
